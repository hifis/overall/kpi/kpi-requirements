# HIFIS KPI Requirements

KPI raw data shall follow a unified format and updating procedure, which is briefly summarized here.

## KPI Repositories for raw data

The KPI data is to be located in separate repositories, as follows:

* Each KPI repository SHOULD be located within the HIFIS KPI group at <https://gitlab.hzdr.de/hifis/overall/kpi>.
* There MUST be one KPI repository per Helmholtz Cloud Service.
* The naming SHOULD follow the convention `<provider_abbreviation>-<Service_Name>`. E.g., `HZB-nubes`.
* The repository MUST be world-public readable for transparency reasons.
    - Consequently, service providers SHOULD pay attention to place only publishable information there.
* Service responsible persons SHOULD take care that write access to these repositories is as restricted as necessary.

## Documentation

*  Each Repository MUST contain a `README.md` file, briefly explaining the content and meaning of the reported.

## Data format

Within each KPI raw data project - representing one service, respectively - , multiple service KPI data and meta data can be given, stored in plain text `.csv` files, data separated in columns, and temporally ordered.

### File names

* All data to be reported MUST be stored in at least one csv file in `stats` folder, e.g. `stats/data.csv`, in `master` or `main` branch.
* The latest commit of that file / these files in that branch will be taken for reporting and plotting. No other branches or forked versions will be considered.

### File content

The `csv` file(s) MUST consist of
- At least two columns with comma (`,`) as separator
- Header lines:
    - One headline, commented by `#`, briefly indicating the meaning of the respective column. This string is directly used for plotting, e.g. on y axes. The first column is always titled `date-time`.
    - One `plot` line, commented by `#`, indicating whether or not the respective column shall be plotted. `1` for plotting, everything else for not-plotting. The latter may be used to integrate e.g. additional conditions or comments for each data point.
    - One `unit` line, commented by `#`, denoting the unit for the numbers given in the respective column. This can be left empty, if `plot!=1`
- Multiple data lines:
    - The first column is always the `date-time` column, with date and time given in RFC-3339 / ISO 8601 format.
        - E.g., `date --rfc-3339=seconds` :arrow_right: `2021-09-17 12:34:48+02:00`, or
        - With `T` as separator :arrow_right: `2021-09-17T12:34:48+02:00`
        - The two variants may also be mixed (not desired).
    - Each other column:
        - If for this column `plot==1`, the content MUST be plain numbers (floating point or integers), i.e. without units, comments, etc.
        - If for this column `plot!=1`, the content can be arbitrary. For example, comments can be included. The meaning of such comment columns SHOULD be explained in `README.md`.

### Data to be plotted

The raw data to be plotted, i.e. the numbers contained in columns defined as `plot==1`, MUST be consistent with the following requirements:

* The data MUST be consistent in time, i.e. `date-time` columns MUST be ordered and the respective data column contents MUST represent the KPI development over time.
* :construction: _How to handle time-window specific usage (e.g. daily users) vs. accumulated KPI_?

### Example
```
#date-time,Number of daily users,comment,source
#plot,1,0
#unit,user,,
2021-09-17 12:37:28+02:00,5,start of service,https://gitlab.hzdr.de/some-documentation-link
2021-09-18 09:00:00+02:00,20,20 new members of workshop,https://gitlab.hzdr.de/some-documentation-link-different-commit
2021-09-19 09:00:00+02:00,0,nothing happening on sunday,
2021-09-20 09:00:00+02:00,3,some more monday morning users,https://gitlab.hzdr.de/some-documentation-link-different-commit
```

Within Gitlab or other md viewers, this will be automatically formatted as readable table, e.g. [data-example.csv](./data-example.csv)

## Data Updating and Plotting

* The data MUST be updated at least once per week (max. 7 days distance between subsequent data points)
* The plotting of data is currently centralized in the project <https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci>.
    * :construction: _Notification upon updates :arrow_right: CI_
    * Note: Updated data is only considered when data file(s) in `master` branch are updated. Side branches are not considered.
* Data updates SHOULD be done such that commit diffs are minimized, so that tracking of changes is facilitated. E.g., please minimize fluctuations of formats between subsequent commits, like introducing and removing spaces, columns, etc.

## Notification in case of Format Changes
* Service providers MUST file an issue or merge request there if data formats change and thus plotting needs to be adapted.
* Service providers MUST inform Helmholtz Cloud Portfolio Management of these changes.  
  :construction: Presumably, this can be most easily performed by inclusion of LMH, ASpi in the above issue/merge request.

## Licensing/usage?
* :construction: _Since data and scripts are public readable: What about licensing?_
